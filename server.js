const express = require('express')
const config = require('./config') //Server config file
const helmet = require('helmet') //HTTP header security
const cors = require('cors') //Ensure requests are coming from trusted sources
const bodyParser = require('body-parser') //Http body parsing
const morgan = require('morgan') //Http request logging 
const jsonwebtoken = require('jsonwebtoken') //JWT Authentication
const bcrypt = require('bcrypt') //Used to encrypt data
const mongoose = require('mongoose') //MongoDb database driver
const app = express()
const router = express.Router()

app.use(helmet())
app.use(cors({
    origin: config.trusted_source,
    optionsSuccessStatus: 200
}))
app.use(bodyParser.urlencoded({ extended: false })) // parse application/x-www-form-urlencoded
app.use(bodyParser.json()) //parse application/json
app.use(morgan('dev'))

//Public and API routes
app.use('/', require('./app/routes/index'))

//Attempt DB Connection 
mongoose.connect(config.database, { useMongoClient: true })
mongoose.connection.once('open', function(){
    console.log('Connected to database')
})
mongoose.connection.on('error', function(error){
    console.log("Failed to connect to database")
    console.log(`${config.name} is shutting down`)
    process.exit()
})

//
app.listen(config.port, function() {
    console.log(`${config.name} is listening on port ${config.port}...`)
})