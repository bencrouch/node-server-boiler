const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = mongoose.model('User')
const config = require('../../config')

exports.register = function(req, res) {
    let newUser = new User(req.body);
    newUser.auth.hash = bcrypt.hashSync(req.body.password, 10)
    bcrypt.hash(req.body.password, 10, function(err, hash) {
        if(err)
            next()
        newUser.auth.hash = hash
        
    })
}

exports.login = function(req, res) {
    
}

exports.loginRequired = function(req, res) {
    
}