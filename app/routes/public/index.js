const express = require('express')
const app = express()
const config = require('../../../config') //Server config file

app.get('/', function (req, res) {
    res.send(`Default path for ${config.name} API`)
})

module.exports = app