const express = require('express')
const router  = express.Router()
const config = require('../../../config') //Server config file

router.get('/', function(req, res) {
    res.json({
        message: `${config.name} API Home`
    })
})

module.exports = router