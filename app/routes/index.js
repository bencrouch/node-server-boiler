const express = require('express')
const app = express()
const config = require('../../config') //Server config file
const router  = express.Router()

//Default path; no authorization required (.com/)
app.use('/', require('./public'))

//Default API path; authorization required within (.com/api_path/)
router.use('/', require('./api'))

//Set the API routes to use api_path from config
app.use(`/${config.api_path}`, router);

app.use(function (err, req, res, next) {
    console.log(err)
    res.status(500).send({"Error": err})
})

module.exports = app