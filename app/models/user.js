const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const config = require('../../config')

let UserSchema = new mongoose.Schem({
    username: { type: String, required: true, trim: true },
    auth: {
        hash: { type: String, required: true },
        salt: String
    }
}, { timestamps: true })

UserSchema.plugin(uniqueValidator, { message: 'User already exists'})

UserSchema.methods.

const User = module.exports = mongoose.model('User', UserSchema)