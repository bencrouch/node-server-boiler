module.exports = {
    name: 'Bogota Server', //Name that will be displayed
    port: 5000, //Port number for server to listen
    database: 'mongodb://104.237.3.67:27017/colombia',
    secret: 'vmDgWNNzOzSmSEi8lNumj6f8cGt3zcih', //Used for JWT
    trusted_source: '',
    api_path: 'api', //ex: .com/{api_path}/
}